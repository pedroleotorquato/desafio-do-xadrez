var tabuleiro = [
                [4,3,2,5,6,2,3,4],
                [1,1,1,1,1,1,1,1],
                [0,0,0,0,0,0,0,0],
                [0,0,0,0,0,0,0,0],
                [0,0,0,0,0,0,0,0],
                [0,0,0,0,0,0,0,0],
                [1,1,1,1,1,1,1,1],
                [4,3,2,5,6,2,3,4]]
/*
Cada indice do array "contagem" representa o codigo de uma peca do tabuleiro. Isto eh, 
o indice 0 representa vazio, indice 1 representa peao, indice 2 representa Bispo etc.
Os valores apontados pelo indices representam, respectivamente, o total de pecas de mesmo codigo 
presentes no tabuleiro.
*/
var contagem = []

/*
Em JavaScript, se a memoria alocada em determinado indice de um array nao for populada,
seu valor padrao eh "undefined". Por isso, faz-se necessario atribuir 0 a todo o array "contagem" para
que no caso de nao haver nenhum "cavalo", por exemplo, apareca 0 na resposta e nao o valor "undefined".
O mesmo procedimento pode ser realizado pelo metodo nativo da linguagem "fill()".
*/
for(var i=0; i<7; i++)
{
    contagem[i] = 0
}

/*
Percorre a matriz "tabuleiro", somando as pecas presentes, passando como indice de contagem
o valor obtido de "tabuleiro[i][j]"
*/
for(var i=0; i<tabuleiro.length; i++)
{
    for(var j=0; j<tabuleiro[i].length; j++)
    {
        contagem[tabuleiro[i][j]] += 1        
    }
}

//Saida
console.log("Peão: " + contagem[1] + " peça(s)")
console.log("Bispo: " + contagem[2] + " peça(s)")
console.log("Cavalo: " + contagem[3] + " peça(s)") 
console.log("Torre: " + contagem[4] + " peça(s)")
console.log("Rainha: " + contagem[5] + " peça(s)")
console.log("Rei: " + contagem[6] + " peça(s)")